---
layout: post
title: What's Jekyll?
description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ornare tincidunt ante aliquet accumsan. Suspendisse pulvinar tortor sit amet dolor consequat, sit amet volutpat sapien aliquet. Suspendisse ac consectetur dolor. Suspendisse eros magna, blandit non vestibulum faucibus, dignissim et justo. Sed id nisi elit. Cras congue quis nulla vel pretium. Integer in malesuada justo. Duis cursus, mi sit amet facilisis porta, ligula tortor hendrerit urna, eget pharetra nisi purus sit amet elit. Suspendisse congue sapien enim, vitae blandit justo malesuada ac. Suspendisse eu vestibulum magna. Nam gravida porta urna, at vehicula nibh bibendum a. Mauris facilisis, erat id suscipit vulputate, augue massa imperdiet justo, eget facilisis orci tortor eget velit. Suspendisse mollis nunc in eros mattis, non luctus justo sollicitudin. "
---

[Jekyll](http://jekyllrb.com) is a static site generator, an open-source tool for creating simple yet powerful websites of all shapes and sizes. From [the project's readme](https://github.com/mojombo/jekyll/blob/master/README.markdown):

  > Jekyll is a simple, blog aware, static site generator. It takes a template directory [...] and spits out a complete, static website suitable for serving with Apache or your favorite web server. This is also the engine behind GitHub Pages, which you can use to host your project’s page or blog right here from GitHub.

It's an immensely useful tool and one we encourage you to use here with Hyde.

Find out more by [visiting the project on GitHub](https://github.com/mojombo/jekyll).